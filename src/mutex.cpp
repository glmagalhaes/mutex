#include "mutex.h"

using namespace std;
using namespace MGMutex;

Mutex::Mutex(){
	OPA_store_int(&this->locked, 0);
}
Mutex::~Mutex(){
	
}
void Mutex::lock(){
  OPA_int_t old;
  do{
    OPA_store_int(&old,OPA_cas_int(&this->locked,0,1));
  }while(OPA_load_int(&old) == 1);
}
void Mutex::unlock(){
  OPA_store_int(&this->locked, 0);
}
