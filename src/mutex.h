#include <opa_primitives.h>

namespace MGMutex{
	class Mutex{
	private:
		OPA_int_t locked;
	public:
		Mutex();
		~Mutex();
		void lock();
		void unlock();
	};
}
