#include <iostream>
#include <queue>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include "mutex.h"

#define NMaxThread 30

using namespace std;
using namespace MGMutex;


pthread_t nthread[NMaxThread];

void *consumidor();
void *produtor();

queue<int> fila;
Mutex atravessador;

int main(int argc, char *argv[]){
	srand (time(NULL));
	int i;
	int ThreadProduct=0;
	int ThreadConsum=0;
	for(i=0 ; i<NMaxThread ; i+=2){
		ThreadProduct = pthread_create(&(nthread[i]), NULL, (void* (*)(void*))produtor , NULL);
		ThreadConsum = pthread_create(&(nthread[i+1]), NULL, (void* (*)(void*))consumidor , NULL);
		if(ThreadProduct != 0){
			cout << "Erro na thread de produtor: " << strerror(ThreadProduct) << endl;
	  	}
		if(ThreadConsum != 0){
			cout << "Erro na thread de consumidor: " << strerror(ThreadConsum) << endl;
		}	
	}
	for(i=0;i<NMaxThread;i++){
		pthread_join(nthread[i],NULL);
	}
	return 0;
}

void *produtor(){
	int temp;
	atravessador.lock();
	temp = rand()%100+1;
	fila.push(temp);
	cout << "Produziu: " << temp << endl;
	atravessador.unlock();
}

void *consumidor(){
	atravessador.lock();
	while(fila.size() == 0){
		atravessador.unlock();
		atravessador.lock();
	}
	cout << "Consumiu: " << fila.front() << endl;
	fila.pop();
	atravessador.unlock();
}
